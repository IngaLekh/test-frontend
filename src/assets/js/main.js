$(function () {
    var apiUrl = 'https://api.punkapi.com/v2/beers';
    var beers = [];
    
    $.ajax({
        url: apiUrl,
        success: successHandler
    });
    
    function successHandler(response) {
        beers = response;
        showBeers(beers);
    }
    
    $('#search-name').keyup(function () {
        var beerName = $(this).val().toLowerCase();
        var filtered = beers.filter(function (beer) {
            return beer.name.toLowerCase().indexOf(beerName) !== -1;
        });
        
        showBeers(filtered);
    });
    
    $('#sort-name').on("click", function () {
        var sortN = beers.sort(function (a, b) {
            return a.name.localeCompare(b.name)
        });
        
        showBeers(sortN);
    });
    
    $('#sort-id').on("click", function () {
        var sortID = beers.sort(function (a, b) {
            return Number(a.id) - Number(b.id);
        });
        
        showBeers(sortID);
    });
    
    $('#sort-tagline').on("click", function () {
        var sortTag = beers.sort(function (a, b) {
            return a.tagline.localeCompare(b.tagline)
        });
        
        showBeers(sortTag);
    });
    
    $('#sort-abv').on("click", function () {
        var sortABV = beers.sort(function (a, b) {
            return a.abv - b.abv;
        });
        
        showBeers(sortABV);
    });
    
    
    function showBeers(beers) {
        $('.allBeer').remove();
        if (beers.length) {
            appendSortedBeers(beers);
            initBeersClickListener();
        }
    }
    
    function appendSortedBeers(beers) {
        $.each(beers, function () {
            $(".beer-list").append("<div class='row allBeer' id='" + this.id + "' data-toggle='modal' data-target='#myModal' ><div class='col-xs-3 '><div class='name'> " + this.name + "</div></div><div class='col-xs-2'><div class='id'> " + this.id + "</div></div><div class='col-xs-4'><div class='tag'>" + this.tagline + "</div></div><div class='col-xs-3'><div class='abv'> " + this.abv + "</div></div></div>");
        });
    }
    
    function initBeersClickListener() {
        $(".allBeer").on("click", function () {
            var id = $(this).attr('id');
            var beer = beers.find(function (beer) {
                return Number(id) === Number(beer.id);
            });
            
            $('.modal-body p').remove();
            $('.modal-body').append('<p>' + beer.description + '</p>');
        });
    }
});

